<?php
class Globals {
  public static $META_DESC_LENGTH_IDEAL = 160; 
  public static $META_KEYWORDS_LENGTH_IDEAL = 160; 
  public static $META_TITLE_LENGTH_IDEAL = 60; 
  public static $META_DESC_LENGTH = 200; 
  public static $META_KEYWORDS_LENGTH = 200; 
  public static $META_TITLE_LENGTH = 70;  
  
  public static $GLOBAL_DASHBOARD = '1AR23_XiOT6wDOsHc_wnIgTAqbqo-fiNt';

 
  public static $GLOBAL_HOME = "/var/www/virtual/coerver.cz/htdocs/www";   //puvodni globalgal
  public static $GLOBAL_TEMP_DIR = '';   
  public static $GLOBAL_BASE_URL = 'http://www.coerver.cz/';     
  public static $GLOBAL_WEB_IMGS_URL = '/web_images/';   
  public static $GLOBAL_WEB_IMGS_PATH = '/var/www/virtual/coerver.cz/htdocs/www/web_images/';   

  public static $GLOBAL_MODULES_BLOG = 'Blog';   
  public static $GLOBAL_MODULES_MEMBERS = 'Trenéři';     
  public static $GLOBAL_ADMIN_PAGER = 15; 

  public static $GLOBAL_SQL_FILE = '/sql2018/db.php'; 

  // pouziti pak Globals::$GLOBAL_SQL_FILE 
 }
 
// echo Globals::$GLOBAL_HOME;
 

?>
