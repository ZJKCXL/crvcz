<?
namespace Tools;

/*
Staticky objekt DateTime je namespace pro vsechny funkce pracujici s casem a datem.
Date pouze s datem a Time pouze s casem;
Behemoth, 2006-31-03

Metoda diffDateTime ma switch na letni cas (default = false). Cela finta je pridani "GMT"
za datum (pr. "2007-03-02 GMT"), aby strtotime nepocitaly s chybejici/prebyvajici
hodinou.
kaspi, 2006-06-04
*/

class DateTimeExt
{
    static public function now($format = "Y-m-d H:i:s") {
        return date($format,time());
    }

    static public function addDateTime ($date, $return_format = "Y-m-d H:i:s", $years = 0, $months = 0, $days = 0, $hours = 0, $minutes = 0, $seconds = 0) {
        $timestamp = mktime(TimeExt::getHours($date)+$hours, TimeExt::getMinutes($date)+$minutes, TimeExt::getSeconds($date)+$seconds, DateExt::getMonth($date)+$months, DateExt::getDay($date)+$days, DateExt::getYear($date)+$years);
        if (($return = date($return_format, $timestamp)) > 0)
        {
            return $return;
        }
    }

    static public function diffDateTime ($in_start, $in_end, $in_daylight_saving = false, $in_thirty_day_month = false) {
        if (!$in_daylight_saving)
        {
            $in_start .= " GMT";
            $in_end .= " GMT";
        }
        $start_stmp = strtotime($in_start);
        $end_stmp = strtotime($in_end);

        $date_start = date("Y-m-d H:i:s", $start_stmp);
        $date_end = date("Y-m-d H:i:s", $end_stmp);

        $mktime_start = $start_stmp;
        $mktime_end = $end_stmp;

        $sign = "";
        if ($mktime_start >  $mktime_end)
        {
            $tmp =  $mktime_end;
            $mktime_end = $mktime_start;
            $mktime_start = $tmp;
            $sign = "-";
        }

        if ($in_thirty_day_month)
        {
            $end = (date("Y", $mktime_end)*360)+(date("m", $mktime_end)*30)+(date("d", $mktime_end));
            $start = (date("Y", $mktime_start)*360)+(date("m", $mktime_start)*30)+(date("d", $mktime_start));

            $days = $end-$start;
        }
        else
        {
            $diff = $mktime_end - $mktime_start;
            $offset = $diff % 86400;
            $days = (floor (($diff - $offset)/86400));

            $diff = $offset;
            $offset = $diff % 3600;
            $hours = (floor (($diff - $offset)/3600));

            $diff = $offset;
            $offset = $diff % 60;
            $minutes = (floor (($diff - $offset)/60));

            $seconds = $offset;
        }

        return array("sign" => $sign,
            "days" => $days,
            "hours" => $hours,
            "minutes" => $minutes,
            "seconds" => $seconds);
    }

    static public function us_to_cz($in_us_datetime, $strip_seconds = false)
    {
        $result = false;

        if (strlen($in_us_datetime) > 10)
        {
            $stmp = strtotime($in_us_datetime);
            if ($strip_seconds) {
                $result = date("j.n.Y H:i",$stmp);
            } else {
                $result = date("j.n.Y H:i:s",$stmp);
            }
        }

        return $result;
    }

    static public function us_to_cz_date($in_us_datetime)
    {
        $result = false;

        if (strlen($in_us_datetime) > 10)
        {
            $stmp = strtotime($in_us_datetime);
            $result = date("j.n.Y",$stmp);
        }

        return $result;
    }

    static public function firstDay($in_month, $in_year) {
        $result = false;

        $stmp = $in_year."-".$in_month."-01 00:00:01";
        $result = date("Y-m-d H:i:s",strtotime($stmp));

        return $result;
    }

    static public function lastDay($in_month, $in_year) {
        $result = false;

        $stmp = $in_year."-".$in_month."-01 00:00:01";
        $result = date("Y-m-t H:i:s",strtotime($stmp));

        return $result;
    }

    static public function convertToDate($in_date_time) {
        $result = false;

        $stmp = strtotime($in_date_time);
        if ($stmp !== false) {
            $result = date("Y-m-d", $stmp);
        }

        return $result;
    }
    static public function convertToTime($in_date_time) {
        $result = false;

        $stmp = strtotime($in_date_time);
        if ($stmp !== false) {
            $result = date("H:i:s", $stmp);
        }

        return $result;
    }

    static public function getIMFDate() {
        $result = false;

        $dateTime = new \DateTime();
        $dateTime->setTimeZone(new \DateTimeZone('GMT'));
        $result = $dateTime->format('D, d M Y H:i:s \G\M\T');

        return $result;
    }

    static public function convertIMFDate($in_date_time) {
        $result = false;

        $dateTime = new \DateTime();
        $dateTime->setTimestamp($in_date_time);
        $dateTime->setTimeZone(new \DateTimeZone('GMT'));
        $result = $dateTime->format('D, d M Y H:i:s \G\M\T');

        return $result;
    }

    static public function parseIMFDate($dateString) {

        $month = '(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)';
        $weekday = '(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)';
        $wkday = '(Mon|Tue|Wed|Thu|Fri|Sat|Sun)';
        $time = '([0-1]\d|2[0-3])(\:[0-5]\d){2}';
        $date3 = $month . ' ([12]\d|3[01]| [1-9])';
        $date2 = '(0[1-9]|[12]\d|3[01])\-' . $month . '\-\d{2}';
        $date1 = '(0[1-9]|[12]\d|3[01]) ' . $month . ' [1-9]\d{3}';

        $asctime_date = $wkday . ' ' . $date3 . ' ' . $time . ' [1-9]\d{3}';
        $rfc850_date = $weekday . ', ' . $date2 . ' ' . $time . ' GMT';
        $rfc1123_date = $wkday . ', ' . $date1 . ' ' . $time . ' GMT';
        $HTTP_date = "($rfc1123_date|$rfc850_date|$asctime_date)";
        $dateString = trim($dateString, ' ');
        if (!preg_match('/^' . $HTTP_date . '$/', $dateString))
            return false;
        if (strpos($dateString, ' GMT') === false)
        $dateString .= ' GMT';
        try {
            return new \DateTime($dateString, new \DateTimeZone('UTC'));
        } catch (\Exception $e) {
            return false;
        }
    }
}

/*
class TimeExt
{
static public function Now($format = "H:i:s")
{
return date($format,time());
}

static public function getHours ($date)
{
return date("H", strtotime($date));
}
static public function getMinutes ($date)
{
return date("i", strtotime($date));
}
static public function getSeconds ($date)
{
return date("s", strtotime($date));
}
}
*/

class MicroTime
{
    public static function now()
    {
        list($usec, $sec) = explode(" ", microtime());
        return $sec."-".str_replace("0.","",$usec);
    }
}

?>