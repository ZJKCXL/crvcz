<?

header("Content-type: text/javascript; charset=utf-8");

include_once("./conf.php");
include_once("./../lib/langs.php");

$tmp_lang = get_lang();
if (@file_exists("./../lang/".$tmp_lang.".php"))
{
	include_once("./../lang/".$tmp_lang.".php");
}

$constants = get_defined_constants(true);
$constants = $constants["user"];

foreach ($constants as $constant_name => $constant_value)
{
	print "var $constant_name = '".addslashes($constant_value)."';";
}

include_once("./conf.protected.php");

?>
