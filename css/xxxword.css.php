<?php
header("Content-type: text/css");
include_once("../config/conf.protected.php");
?> 
 

body{
	background: #1d1e1f url('../images/bodyin.gif') repeat-y center top;
	color: #dbdbdb;
	font-size: 80%  ;
  font-family: "Trebuchet MS",Arial,lucida,sans-serif;
}
strong, b{
  color: #fff;
}
a{
  color: #f2f2f2;
}
a:hover{
 color: #c1001f;
 color: #E00025; 
 text-decoration: none;
}
p {margin:0; padding:0;}
pre {

	font-size: 13px;
}
h2{
 color: #0099CB;
 font-size: 130%;
font-weight: normal;
}
h3{
color: #c1001f;
 color: #E00025;
font-size: 120%;
margin-bottom: 0;
margin-top: 0;
}
h4{
color: #c1001f;
font-size: 110%;
margin-bottom: 0;

}
.imgleft{
 float: left;
 margin: 0 1em 1em 0;
 padding: 2px;
 border: 3px solid #000000; 
}
.imgright{
 float: right;
 margin: 0 0 1em 1em;
  padding: 2px;
 border: 3px solid #000000;
}

.righthalf{
 width: 48%;
 float: right;
}
.lefthalf{
 width: 48%;
 float: left;
}
#text td{
vertical-align:top;
}
ul{
 list-style-type: square;
}
li{
 color: #02b4aa;
}
li span{
 	color: #010101;
}
form{
 clear: left;
 margin: 20px 0;
 background: #EDF2F2;
 padding: 10px;
}
fieldset{
 border: none;
 margin-bottom: 0.5em;
}
input{
 border: 1px solid #C4F2F2
}
textarea{
 border: 1px solid #C4F2F2;
 width: 250px;
 height: 150px;
 overflow: auto;
   font-family:  "Arial CE", "Helvetica CE",  Arial, lucida, sans-serif;
}
#hideform{
 width: 50%;
 display: none;
}




#langs {
 right: 50px;
 top: 10px;
 height: 56px;
 width: 85px;
 position: absolute;
}
#langs  span, #langs  p.span {
	width: 16px;
	height: 11px;
	display: block;
	float: right;
	margin-top: 22px;
	overflow: hidden;
	cursor: pointer;
	margin-right: 4px;
}
#langs span em, #langs  span a {
	display: block;
	width: 16px;
	height: 11px;
	font-size: 9px;
	line-height: 12px;
	z-index: 1;
	cursor: pointer;
}
#langs  span a, #langs  span em.active {
	background:   url('../images/gb.jpg') no-repeat 0px 0px;
	z-index: 10;
	text-decoration: none;
}
#langs  span.cz a, #langs  span.cz em.active {
	background: url('../images/cz.jpg') no-repeat 0px 0px;
}
#langs span em.active, #langs  span.cz em.active  {
  cursor: default;
 }
