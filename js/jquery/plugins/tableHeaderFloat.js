(function( $ ) {
	var methods = {
		init: function (options) {
			$(document).scroll(function() {
				$.fn.tableScroll("handleScroll");
			})
		},
		handleScroll: function () {
			var scroll_x = $(document).scrollLeft();
			var scroll_y = $(document).scrollTop();
			
			$(".tableScroll thead").each(function () {
				if (scroll_y >= $(this).parent().position().top-15.4)
				{
					if (!$(this).hasClass("tableScrollScrolling"))
					{
						var tr = $("tbody tr", $(this).parent()).get(0);
					
						var i = 0;
						$("th", $(this)).each(function () {
							var td = $("td", tr).get(i);
						
							var tmp_width = $(td).width();
							var tmp_padding_x = $(td).innerWidth()-tmp_width;
						
							$(this).css({
								width: tmp_width,
								"padding-left": tmp_padding_x/2,
								"padding-right": tmp_padding_x/2
							});
							
							$(td).css({
								"width": tmp_width
							});

							i++;
						});
					
						$(this).css({
							position: "fixed",
							"_position": "absolute",
							top: "2em",
							left: $(this).position().left+"px",
							"z-index": 2
						});
						$(this).addClass("tableScrollScrolling");
					}
				}
				else
				{
					$(this).css({
						position: "relative",
						"z-index": 1
					});
					
					$(this).removeClass("tableScrollScrolling");
				}
			});
		}
	};
	$.fn.tableScroll = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error("Method "+method+" does not exist on jQuery.overlay");
		}
	};
})( jQuery );

$.fn.tableScroll("init");