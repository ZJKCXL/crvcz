(function( $ ) {
	var methods = {
		fadeWith: function (params) {
			if ($("#statusBar").length > 0)
			{
				$("#statusBar").remove();
			}
			
			var settings = {
				fadeLength: 3000
			};
			
			$.extend(settings, params);
			
			$("<div>")
			.html(settings.html)
			.attr("id", "statusBar")
			.addClass("inlineBlock")
			.appendTo($("body"));
			
			$("#statusBar")
			.css({
				position: "fixed",
				".position": "absolute",
				top: 0,
				left: "50%",
				"margin-left": "-"+($("#statusBar").outerWidth()/2)+"px",
				"z-index": 110
			})
			.fadeToggle(settings.fadeLength, function() {
				$("#statusBar").remove();
			});
		},
		showWith: function (params) {
			if ($("#statusBar").length > 0)
			{
				$("#statusBar").remove();
			}
			
			$("<div>")
			.html(params.html)
			.attr("id", "statusBar")
			.addClass("inlineBlock")
			.appendTo($("body"));
			
			$("#statusBar")
			.css({
				position: "fixed",
				".position": "absolute",
				top: 0,
				left: "50%",
				"margin-left": "-"+($("#statusBar").outerWidth()/2)+"px",
				"z-index": 110
			});
		},
		hide: function (params) {
			$("#statusBar").remove();
		}
	};
	$.fn.statusBar = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error("Method "+method+" does not exist on jQuery.cookie");
		}
	};
})( jQuery );