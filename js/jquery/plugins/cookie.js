(function( $ ) {
	var methods = {
		set: function (params) {
			if (params.name && params.name.length > 0)
			{
				var tmp_cookie = params.name+"=";
				if (params.value && params.value.length > 0)
				{
					tmp_cookie += params.value;
				}
				tmp_cookie += ";";
				if (params.path && params.path.length > 0)
				{
					tmp_cookie += "path="+params.path+";";
				}
				if (params.domain && params.domain.length > 0)
				{
					tmp_cookie += "domain="+params.domain+";";
				}
				if (params.expires && params.expires.length > 0)
				{
					var tmp_expires = new Date(date.getTime()+params.expires*1000).toGMTString();
					tmp_cookie += "expires="+tmp_expires+";";
				}
				if (params.secure && params.secure == 1)
				{
					tmp_cookie += "secure;";
				}
				
				return document.cookie = tmp_cookie;
			}
		},
		valByName: function (cookieName) {
			var result = null;
			
			if (document.cookie && document.cookie.length > 0) {
				cookies = [];
				
				var re = new RegExp(cookieName+"[\=|](.*)");
				var tmp_cookies = document.cookie.split(';');
				for (var i = 0; i < tmp_cookies.length; i++)
				{
					var res = tmp_cookies[i].match(re);
					if (res != null)
					{
						result = res[1];
						break;
					}
				}
			}
			
			return result;
		},
		delAll: function () {
			if (document.cookie && document.cookie.length > 0) {
				cookies = [];
				
				var re = new RegExp("([a-zA-Z]{1,})[\=|].*");
				var tmp_cookies = document.cookie.split(';');
				for (var i = 0; i < tmp_cookies.length; i++)
				{
					var res = tmp_cookies[i].match(re);
					$(this).cookies("delByName", res[1]);
				}
			}
		},
		delByName: function (cookieName) {
			if (document.cookie && document.cookie.length > 0) {
				if ($().cookies("valByName", cookieName) != null)
				{
					document.cookie = cookieName+"=;expires="+new Date(0).toUTCString()+";";
				}
			}
		}
	};
	$.fn.cookies = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error("Method "+method+" does not exist on jQuery.cookie");
		}
	};
})( jQuery );