(function( $ ) {
	var methods = {
		desaturate: function (collection) {
			$(collection).each(function () {
				var imgW = $(this).width();
				var imgH = $(this).height();
				
				if (imgW <= 0 || imgH <= 0)
				{
					return;
				}
				
				var imgObj = $(this).get(0);
				
				if ($.browser.msie)
				{
					imgObj.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(grayScale=1)';
					return;
				}
				
				var canvas = document.createElement("canvas");				
				var canvasContext = canvas.getContext('2d');

				canvas.width = imgW;
				canvas.height = imgH;
				
				canvasContext.drawImage(imgObj, 0, 0);
				
				var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
				
				for (var y = 0; y < imgPixels.height; y++)
				{
					for(var x = 0; x < imgPixels.width; x++)
					{
						var i = (y * 4) * imgPixels.width + x * 4;
						var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
						
						imgPixels.data[i] = avg;
						imgPixels.data[i + 1] = avg;
						imgPixels.data[i + 2] = avg;
					}
				}
				canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);

				$(this).attr("src", canvas.toDataURL());
			});
		}
	};
	$.fn.desaturate = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		}
		else if (typeof method === "object" || !method) {
			return methods.init.apply(this, arguments);
		}
		else {
			$.error("Method "+method+" does not exist on jQuery.overlay");
		}
	};
})( jQuery );