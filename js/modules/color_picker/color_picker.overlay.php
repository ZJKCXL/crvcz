<?

header("Content-type: text/html; charset=iso-8859-2");

$dir = substr($_SERVER['SCRIPT_FILENAME'], strlen($_SERVER['DOCUMENT_ROOT']));
$dir = substr($dir, 0, strpos($dir, "/", 1)+1);

include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.protected.php");
include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.php");
include_once(BASE_DIR."js/modules/color_picker/color_picker.colors.php");

?>
<div id="color_picker_hide"></div>
<?
if (is_array($websafe_colors))
{
	$i = 0;
	foreach ($websafe_colors as $color)
	{
		echo $i%18 == 0 ? "<br />" : "";
?>
	<a href="#" id="<? echo $color; ?>" title="#<? echo $color; ?>" class="color_table background_<? echo $color; ?>" style="background: #<? echo $color; ?>">&nbsp;</a>
<?
		$i++;
	}
}
?>
<br />
<div id="color_selection">&nbsp;</div>
<input type="text" id="color_code" name="color_code" size="6" disabled />