/**
 * Define class and common properties
 */

var calendar = {};

calendar.month = [];
calendar.days = [];
calendar.caption_close = [];
calendar.caption_clear = [];
calendar.date_format = [];
calendar.language = [];
calendar.show_clear = [];
calendar.backward_select = [];
calendar.select_action = [];
calendar.on_year = [];
calendar.on_month = [];
calendar.includes = [];
calendar.params = [];
calendar.invoked_by = null;

/**
 * Set the default bahavior
 */

calendar.date_format.defaults = 'Y-m-d';
calendar.language.defaults = 'cz';
calendar.show_clear.defaults = true; //show the clear button, that will clear the data from the input element
calendar.backward_select.defaults = true; //if user can pick a date that has already passed
calendar.select_action.defaults = []; //functions to run when a date is picked
calendar.includes.defaults = []; //lib files to include in overlay

/**
 * define calendar overlay url and img
 */

calendar.source = BASE_URL+'js/modules/calendar/calendar.overlay.php';
calendar.img = BASE_URL+'images/icons/basic/24/Calendar.png';

/**
 * define initial date format
 */

calendar.format = calendar.date_format.defaults;

/**
 * custom settings for elements
 * calendar.show_clear.<element_id> = true/false;
 * calendar.backward_select.<element_id> = true/false;
 * calendar.select_action.<element_id> = ['<function_name>', '<function_name>', ...];
 * calendar.on_year.<element_id> = int;
 * calendar.on_month.<element_id> = int;
 * calendar.includes.<element_id> = ['file1', 'file2', ...];
 * calendar.params.<element_id> = ['param1', 'param2', ...];
 */

calendar.show_clear.checkin = false;
calendar.select_action.checkin = ['booking.updateCheckout'];
calendar.select_action.checkout = ['booking.updateCheckout'];
calendar.show_clear.checkout = false;

/**
 * default language settings
 * + english language settings
 */

calendar.month.defaults = [];
calendar.month.defaults[1] = "January";
calendar.month.defaults[2] = "February";
calendar.month.defaults[3] = "March";
calendar.month.defaults[4] = "April";
calendar.month.defaults[5] = "May";
calendar.month.defaults[6] = "June";
calendar.month.defaults[7] = "July";
calendar.month.defaults[8] = "August";
calendar.month.defaults[9] = "September";
calendar.month.defaults[10] = "October";
calendar.month.defaults[11] = "November";
calendar.month.defaults[12] = "December";

calendar.days.defaults = [];
calendar.days.defaults[1] = "Mo";
calendar.days.defaults[2] = "Tu";
calendar.days.defaults[3] = "We";
calendar.days.defaults[4] = "Th";
calendar.days.defaults[5] = "Fr";
calendar.days.defaults[6] = "Sa";
calendar.days.defaults[7] = "Su";

calendar.caption_close.defaults = [];
calendar.caption_close.defaults = "close";
calendar.caption_clear.defaults = [];
calendar.caption_clear.defaults = "clear";

calendar.date_format.en = calendar.date_format.defaults;
calendar.month.en = calendar.month.defaults;
calendar.days.en = calendar.days.defaults;
calendar.caption_close.en = calendar.caption_close.defaults;
calendar.caption_clear.en = calendar.caption_clear.defaults;

/**
 * language settings - add languages as you need
 */

calendar.month.cz = [];
calendar.month.cz[1] = "Leden";
calendar.month.cz[2] = "Únor";
calendar.month.cz[3] = "Březen";
calendar.month.cz[4] = "Duben";
calendar.month.cz[5] = "Květen";
calendar.month.cz[6] = "Červen";
calendar.month.cz[7] = "Červenec";
calendar.month.cz[8] = "Srpen";
calendar.month.cz[9] = "Září";
calendar.month.cz[10] = "Říjen";
calendar.month.cz[11] = "Listopad";
calendar.month.cz[12] = "Prosinec";

calendar.days.cz = [];
calendar.days.cz[1] = "Po";
calendar.days.cz[2] = "Út";
calendar.days.cz[3] = "St";
calendar.days.cz[4] = "Čt";
calendar.days.cz[5] = "Pá";
calendar.days.cz[6] = "So";
calendar.days.cz[7] = "Ne";

calendar.caption_close.cz = [];
calendar.caption_close.cz = "zavřít";
calendar.caption_clear.cz = [];
calendar.caption_clear.cz = "vymazat";

calendar.date_format.cz = 'd.m.Y';

calendar.init = function ()
{
	calendar.language.defaults = LANGUAGE;
	
	var inputs = document.getElementsByTagName('input');

	for (var i = 0; i < inputs.length; i++)
	{
		if (class_handler.has(inputs[i], 'date'))
		{
			event_handler.remove(inputs[i], 'click', calendar.invoke);
			event_handler.add(inputs[i], 'click', calendar.invoke);

			inputs[i].style.backgroundImage = 'url('+calendar.img+')';
			inputs[i].style.backgroundPosition = 'center right';
			inputs[i].style.backgroundRepeat = 'no-repeat';
		}
	}

	var calendar_div = document.createElement('div');
	calendar_div.id = 'javascript_calendar';
	calendar_div.style.position = 'absolute';
	calendar_div.style.zIndex = '102';
	calendar_div.style.display = 'none';

	if (document.body)
	{
		document.body.appendChild(calendar_div);
	}
	else if (document.documentElement)
	{
		document.documentElement.appendChild(calendar_div);
	}

	event_handler.remove(window, 'click', calendar.windowClick);
	event_handler.add(window, 'click', calendar.windowClick);

	calendar.setDateFormat();
};
calendar.invoke = function (event)
{
	calendar.destroy();

	var event = event_handler.fix(event);
	if (event.preventDefault)
	{
		event.preventDefault();
	}

	var element = event.target;

	if (element.tagName.toLowerCase() == 'img')
	{
		element = document.getElementById(element.id.substr(12, event.target.id.length));
	}

	calendar.invoked_by = element;
	calendar.setDateFormat(calendar.invoked_by.id);

	var param = [];
	var p = 0;

	param[p] = ['element_id', element.id];
	p++;

	param[p] = ['language', calendar.language];
	p++;

	var tmp_lang = calendar.language.defaults;
	if (eval('typeof(calendar.language.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_lang = eval('calendar.language.'+calendar.invoked_by.id);
	}

	param[p] = ['lang', tmp_lang];
	p++;

	var tmp_clear = calendar.show_clear.defaults;
	if (eval('typeof(calendar.show_clear.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_clear = eval('calendar.show_clear.'+calendar.invoked_by.id);
	}

	param[p] = ['show_clear', tmp_clear];
	p++;

	var tmp_backward = calendar.backward_select.defaults;
	if (eval('typeof(calendar.backward_select.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_backward = eval('calendar.backward_select.'+calendar.invoked_by.id);
	}

	param[p] = ['backward_select', tmp_backward];
	p++;

	if (eval('typeof(calendar.on_month.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_on_month = eval('calendar.on_month.'+calendar.invoked_by.id);
		param[p] = ['on_month', tmp_on_month];
		p++;
	}

	if (eval('typeof(calendar.on_year.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_on_year = eval('calendar.on_year.'+calendar.invoked_by.id);
		param[p] = ['on_year', tmp_on_year];
		p++;
	}

	var tmp_includes = calendar.includes.defaults;
	if (eval('typeof(calendar.includes.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_includes = eval('calendar.includes.'+calendar.invoked_by.id);
	}

	param[p] = ['includes', tmp_includes];
	p++;

	if (eval('typeof(calendar.params.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_params = eval('calendar.params.'+calendar.invoked_by.id);
		for (var i = 0; i < tmp_params.length; i++)
		{
			param[p] = [tmp_params[i][0], eval(tmp_params[i][1])];
			p++;
		}
	}

	param[p] = ['format', calendar.format];
	p++;

	if (element.value.length > 0)
	{
		param[p] = ['date', element.value];
		p++;
	}

	var requester = new xmlhttp_handler;
	if (requester.loadURL('POST', calendar.source, param, false))
	{
		var content = requester.getText();

		var calendar_div = document.getElementById('javascript_calendar');
		
		calendar_div.innerHTML = content;
		
		var tmpY = position.getPositionY(event)+20;
		if (tmpY+$("#javascript_calendar").outerHeight(true)>$(window).height()+position.getScrollY(event))
		{
			tmpY = $(window).height()+position.getScrollY(event)-$("#javascript_calendar").outerHeight(true);
		}
		
		var tmpX = position.getPositionX(event);
		if (tmpX+$("#javascript_calendar").outerWidth(true) >$(window).width()+position.getScrollX(event))
		{
			tmpX = $(window).width()+position.getScrollX(event)-$("#javascript_calendar").outerWidth(true);
		}
		
		calendar_div.style.display = 'block';
		calendar_div.style.top = tmpY+'px';
		calendar_div.style.left = tmpX+'px';

		var anchors = calendar_div.getElementsByTagName('a');
		for (var i = 0; i < anchors.length; i++)
		{
			if (class_handler.has(anchors[i], 'close_cal'))
			{
				event_handler.add(anchors[i], 'click', calendar.destroy);
			}
			if (class_handler.has(anchors[i], 'clear_cal'))
			{
				event_handler.add(anchors[i], 'click', calendar.clear);
			}
			if (class_handler.has(anchors[i], 'fill_date'))
			{
				event_handler.add(anchors[i], 'click', calendar.fill_date);
			}
			else if (class_handler.has(anchors[i], 'move'))
			{
				event_handler.add(anchors[i], 'click', calendar.move);
			}
		}

		var selects = calendar_div.getElementsByTagName('select');
		for (var i = 0; i < selects.length; i++)
		{
			if (class_handler.has(selects[i], 'move'))
			{
				event_handler.add(selects[i], 'change', calendar.move);
			}
		}
	}
};
calendar.destroy = function (event)
{
	if (event)
	{
		var event = event_handler.fix(event);

		if (event.preventDefault)
		{
			event.preventDefault();
		}
	}

	var calendar_div = document.getElementById('javascript_calendar');

	if (calendar_div)
	{
		calendar_div.innerHTML = '';
		calendar_div.style.display = 'none';
	}

	calendar.invoked_by = null;

	return false;
};
calendar.fill_date = function (event)
{
	var event = event_handler.fix(event);

	if (event.preventDefault)
	{
		event.preventDefault();
	}

	var tmp_fill = event.target.id;
	var tmp_date = date_handler.convert(tmp_fill, "Y-m-d", calendar.format);

	calendar.invoked_by.value = tmp_date;

	var tmp_select_actions = (dom_handler.getProperties(calendar.select_action)) ? dom_handler.getProperties(calendar.select_action) : 0;
	if (tmp_select_actions.length > 0)
	{
		var has_run = false;

		for (var i = 0; i < tmp_select_actions.length; i++)
		{
			if (calendar.invoked_by.id == tmp_select_actions[i])
			{
				var tmp_functions = eval('calendar.select_action.'+tmp_select_actions[i]);
				if (tmp_functions.length > 0)
				{
					for (var j = 0; j < tmp_functions.length; j++)
					{
						var tmp_class = tmp_functions[j].split('.');
						if (tmp_class.length > 0)
						{
							if (eval('typeof('+tmp_class[0]+')') == 'undefined')
							{
								continue;
							}
						}

						if (eval('typeof('+tmp_functions[j]+')') != 'undefined')
						{
							var tmp_run = eval(tmp_functions[j]);
							tmp_run();
						}
					}
				}

				has_run = true;
			}
		}

		if (!has_run)
		{
			var tmp_functions = calendar.select_action.defaults;
			if (tmp_functions.length > 0)
			{
				for (var j = 0; j < tmp_functions.length; j++)
				{
					var tmp_class = tmp_functions[j].split('.');
					if (tmp_class.length > 0)
					{
						if (eval('typeof('+tmp_class[0]+')') == 'undefined')
						{
							continue;
						}
					}

					if (eval('typeof('+tmp_functions[j]+')') != 'undefined')
					{
						var tmp_run = eval(tmp_functions[j]);
						tmp_run();
					}
				}
			}
		}
	}

	calendar.invoked_by.focus();
	calendar.invoked_by.blur();
	calendar.destroy();

	return false;
};
calendar.clear = function (event)
{
	var event = event_handler.fix(event);

	if (event.preventDefault)
	{
		event.preventDefault();
	}

	calendar.invoked_by.value = '';

	calendar.destroy();

	return false;
};
calendar.move = function (event)
{
	var event = event_handler.fix(event);

	if (event.preventDefault)
	{
		event.preventDefault();
	}

	if (event.cancelBubble)
	{
		event.cancelBubble = true;
	}
	if (event.stopPropagation)
	{
		event.stopPropagation();
	}

	var selected_date = event.target.id;
	if (event.target.tagName.toLowerCase() == 'select')
	{
		selected_date = event.target.value;
	}

	var param = [];
	var p = 0;

	param[p] = ['element_id', calendar.invoked_by.id];
	p++;

	param[p] = ['language', calendar.language];
	p++;

	var tmp_lang = calendar.language.defaults;
	if (eval('typeof(calendar.language.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_lang = eval('calendar.language.'+calendar.invoked_by.id);
	}

	param[p] = ['lang', tmp_lang];
	p++;

	var tmp_clear = calendar.show_clear.defaults;
	if (eval('typeof(calendar.show_clear.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_clear = eval('calendar.show_clear.'+calendar.invoked_by.id);
	}

	param[p] = ['show_clear', tmp_clear];
	p++;

	var tmp_backward = calendar.backward_select.defaults;
	if (eval('typeof(calendar.backward_select.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_backward = eval('calendar.backward_select.'+calendar.invoked_by.id);
	}

	param[p] = ['backward_select', tmp_backward];
	p++;

	var tmp_includes = calendar.includes.defaults;
	if (eval('typeof(calendar.includes.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_includes = eval('calendar.includes.'+calendar.invoked_by.id);
	}

	param[p] = ['includes', tmp_includes];
	p++;

	if (eval('typeof(calendar.params.'+calendar.invoked_by.id+')') != 'undefined')
	{
		tmp_params = eval('calendar.params.'+calendar.invoked_by.id);
		for (var i = 0; i < tmp_params.length; i++)
		{
			param[p] = [tmp_params[i][0], eval(tmp_params[i][1])];
			p++;
		}
	}

	param[p] = ['format', calendar.format];
	p++;

	param[p] = ['date', selected_date];
	p++;

	var requester = new xmlhttp_handler;
	if (requester.loadURL('POST', calendar.source, param, false))
	{
		var content = requester.getText();

		var calendar_div = document.getElementById('javascript_calendar');
		calendar_div.innerHTML = '';
		calendar_div.innerHTML = content;

		var anchors = calendar_div.getElementsByTagName('a');
		for (var i = 0; i < anchors.length; i++)
		{
			if (class_handler.has(anchors[i], 'close_cal'))
			{
				event_handler.add(anchors[i], 'click', calendar.destroy);
			}
			if (class_handler.has(anchors[i], 'clear_cal'))
			{
				event_handler.add(anchors[i], 'click', calendar.clear);
			}
			if (class_handler.has(anchors[i], 'fill_date'))
			{
				event_handler.add(anchors[i], 'click', calendar.fill_date);
			}
			if (class_handler.has(anchors[i], 'move'))
			{
				event_handler.add(anchors[i], 'click', calendar.move);
			}
		}

		var selects = calendar_div.getElementsByTagName('select');
		for (var i = 0; i < selects.length; i++)
		{
			if (class_handler.has(selects[i], 'move'))
			{
				event_handler.add(selects[i], 'change', calendar.move);
			}
		}
	}

	return false;
};
calendar.setDateFormat = function (element_id)
{
	if (eval('typeof(calendar.date_format.'+calendar.language.defaults+')') != 'undefined')
	{
		calendar.format = eval('calendar.date_format.'+calendar.language.defaults);
	}
	else if (typeof(element_id) != 'undefined' && eval('typeof(calendar.date_format.'+element_id+')') != 'undefined')
	{
		calendar.format = eval('calendar.date_format.'+element_id);
	}
	else
	{
		calendar.format = calendar.date_format.defaults;
	}
};
calendar.windowClick = function (event)
{
	event = event_handler.fix(event);

	var element = event.target;
	if (element != calendar.invoked_by && calendar.invoked_by != null && element.id != 'cal_move_year'
	&& element.id != 'cal_move_month')
	{
		calendar.destroy();
	}
};