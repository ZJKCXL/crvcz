<?

header("Content-type: text/html; charset=iso-8859-2");

$dir = substr($_SERVER['SCRIPT_FILENAME'], strlen($_SERVER['DOCUMENT_ROOT']));
$dir = substr($dir, 0, strpos($dir, "/", 1)+1);
 $dir = "/";


include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.protected.php");
include_once($_SERVER['DOCUMENT_ROOT'].$dir."config/conf.php");
include_once(BASE_DIR."js/modules/calendar/calendar.languages.php");

if (@file_exists(BASE_DIR."lang/".$_REQUEST["lang"].".php"))
{
	include_once(BASE_DIR."lang/".$_REQUEST["lang"].".php");
}

if (isset($_REQUEST["includes"]))
{
	$_REQUEST["includes"] = explode(",", $_REQUEST["includes"]);
	foreach ($_REQUEST["includes"] as $inc)
	{
		if (@file_exists(BASE_DIR."lib/".$inc.".php"))
		{
			include_once(BASE_DIR."lib/".$inc.".php");
		}
	}
}

/**
 * Set params
 */
$show_clear = ($_REQUEST["show_clear"] == "false") ? false : true;
$backward_select = ($_REQUEST["backward_select"] == "false") ? false : true;
$date_format = $_REQUEST["format"];

/**
 * Set language
 */
$lang = $_REQUEST["lang"];

/**
 * Helper function - get days in month for
 *
 * @param int $month
 * @param int $year
 * @return int
 */
function getDaysInMonth ($month, $year)
{
	$days_in_month = array(1 => 31, 2 => 28, 3 => 31, 4 => 30, 5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31
												, 11 => 30, 12 => 31);
	if ($month != 2) return $days_in_month[intval($month)];
	return (checkdate($month, 29, $year)) ? 29 : 28;
}

/**
 * Get the number of the first day of month (starting with monday)
 *
 * @param int $month
 * @param int $year
 * @return int
 */
function getFirstDay ($month, $year)
{
	$time_stamp = mktime(0, 0, 0, $month, 1, $year);
	$day_arr = getdate($time_stamp);
	return (($day_arr["wday"] == 0) ? 7 : $day_arr["wday"]);
}

/**
 * set up a php format derived from formats like Y-m-d
 */
preg_match_all("/[\w]+/", $date_format, $matches);
$tmp_date = preg_split("/[^\w]/", $_REQUEST["date"]);

if (@count($matches) > 0)
{
	foreach ($matches[0] as $key => $match)
	{
		switch ($match)
		{
			case "d":
				$newdate["day"] = str_pad($tmp_date[$key], 2, 0, STR_PAD_LEFT);
				break;
			case "m":
				$newdate["month"] = str_pad($tmp_date[$key], 2, 0, STR_PAD_LEFT);
				break;
			case "Y":
				$newdate["year"] = $tmp_date[$key];
				break;
		}
	}

	$_REQUEST["date"] = $newdate["year"]."-".$newdate["month"]."-".$newdate["day"];
}

/**
 * Set starting month/year
 */
if (strlen($_REQUEST["on_year"]) > 0 || strlen($_REQUEST["on_month"]) > 0)
{
	$_REQUEST["on_year"] = strlen($_REQUEST["on_year"]) > 0 ? $_REQUEST["on_year"] : date("Y");
	$_REQUEST["on_month"] = strlen($_REQUEST["on_month"]) > 0 ? $_REQUEST["on_month"] : date("m");

	$stmp = mktime(0, 0, 0, $_REQUEST["on_month"], date("d"), $_REQUEST["on_year"]);
}

/**
 * set up initial date
 */
$year = date("Y");
$month = date("m");
$day = date("d");

if (strlen($_REQUEST["date"]) > 0)
{
	$stmp_date = strtotime($_REQUEST["date"]);
	$year = date("Y", $stmp_date);
	$month = date("m", $stmp_date);
	$day = date("d", $stmp_date);
}

/**
 * init values - year select
 */
$start_year = (!$backward_select) ? $year : $year-5;

/**
 * init values - display backward movement elements
 */
$display_previous_month = true;
$display_previous_year = true;

$temp_previous_year = ($month-1 <= 0) ? $year-1 : $year;
$temp_previous_month = ($month-1 <= 0) ? 12 : $month-1;
$temp_previous_day = (getDaysInMonth($temp_previous_month, $temp_previous_year) < $day) ? getDaysInMonth($temp_previous_month, $temp_previous_year) : $day;

$temp_next_year = ($month+1 > 12) ? $year+1 : $year;
$temp_next_month = ($month+1 > 12) ? 1 : $month+1;
$temp_next_day = (getDaysInMonth($temp_next_month, $temp_next_year) < $day) ? getDaysInMonth($temp_next_month, $temp_next_year) : $day;

$previous_month = date($date_format, mktime(0, 0, 0, $temp_previous_month, $temp_previous_day, $temp_previous_year));
$next_month = date($date_format, mktime(0, 0, 0, $temp_next_month, $temp_next_day, $temp_next_year));

$temp_previous_month = sprintf("%02d", $temp_previous_month);
$temp_previous_day = sprintf("%02d", $temp_previous_day);

if (!$backward_select
&& strtotime($temp_previous_year."-".$temp_previous_month."-".$temp_previous_day) < strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y")))))
{
	$display_previous_month = false;
}

$temp_previous_day = (getDaysInMonth(intval($month), $year-1) < $day) ? getDaysInMonth(intval($month), $year-1) : $day;
$temp_next_day = (getDaysInMonth(intval($month), $year+1) < $day) ? getDaysInMonth(intval($month), $year+1) : $day;
$previous_year = date($date_format, mktime(0, 0, 0, intval($month), $temp_previous_day, $year-1));
$next_year = date($date_format, mktime(0, 0, 0, intval($month), $temp_next_day, $year+1));

if (!$backward_select
&& strtotime(($year-1)."-".$month."-".$temp_previous_day) >= strtotime(date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y")))))
{
	$start_year = $year-1;
	$display_previous_year = false;
}

/**
 * Trip info
 */

if (@count($_REQUEST["includes"]) > 0)
{
	if (in_array("order", $_REQUEST["includes"]))
	{
		//$capacity = termin_get_free($year, $month);
		$capacity = segways_get_free($year, $month);
	}
}

?>
<table>
	<thead>
		<tr>
			<th colspan="7">
				<select id="cal_move_year" class="move">
<?
for ($y = $start_year; $y < $year+5; $y++)
{
	$selected = $y == $year ? " selected" : "";
?>
					<option value="<? echo date($date_format, mktime(0, 0, 0, $month, $day, $y));; ?>"<? echo $selected; ?>><? echo $y; ?></option>
<?
}
?>
				</select>
				<select id="cal_move_month" class="move">
<?

for ($m = 1; $m <= 12; $m++)
{
	$selected = $m == $month ? " selected" : "";
?>
					<option value="<? echo date($date_format, mktime(0, 0, 0, $m, $day, $year)); ?>"<? echo $selected; ?>><? echo $months[$lang][$m]; ?></option>
<?
}
?>
				</select>
				<? echo $start_month; ?>
<?
if ($display_previous_month)
{
?>
				<a href="#" id="<? echo $previous_month; ?>" class="move float_left" title="<? echo $back[$lang]; ?>">&#171;</a>
<?
}
else
{
?>
				<span class="float_left">&nbsp;</span>
<?
}
?>
				<a href="#" id="<? echo $next_month; ?>" class="move float_right" title="<? echo $forth[$lang]; ?>">&#187;</a>
			</th>
		</tr>
		<tr>
			<th class="dayname"><? echo $days[$lang][1]; ?></th>
			<th class="dayname"><? echo $days[$lang][2]; ?></th>
			<th class="dayname"><? echo $days[$lang][3]; ?></th>
			<th class="dayname"><? echo $days[$lang][4]; ?></th>
			<th class="dayname"><? echo $days[$lang][5]; ?></th>
			<th class="dayname"><? echo $days[$lang][6]; ?></th>
			<th class="dayname"><? echo $days[$lang][7]; ?></th>
		</tr>
	</thead>
	<tbody>
		<tr>
<?
$max_days = getDaysInMonth($month, $year);
$first_day = getFirstDay($month, $year);

$n = 0;
for ($i = 1; $i < $first_day; $i++)
{
	echo "					<td>&nbsp;</td>\n";
	$n++;
}
for ($i = 1; $i < $max_days+1; $i++)
{
	$n++;
	$selected = ($day == $i) ? " selected" : "";
	$id = date("Y-m-d", mktime(0, 0, 0, $month, $i, $year));
	if (!$backward_select
	&& strtotime($year."-".$month."-".$i) < strtotime(date("Y-m-d")))
	{
?>
			<td class="non_selectable"><? echo $i; ?></td>
<?
	}
	else
	{
		if (isset($capacity))
		{
			if ($capacity[$id] > 0)
			{
?>
			<td><a href="#" id="<? echo $id; ?>" title="<? echo TXT_CALENDAR_TRIP_FREE; ?>" class="trips fill_date<? echo $selected; ?>"><? echo $i; ?></a></td>
<?
			}
			else
			{
?>
			<td class="no_trips" title="<? echo TXT_CALENDAR_TRIP_NONE; ?>"><? echo $i; ?></td>
<?
			}
		}
		else
		{
?>
			<td><a href="#" id="<? echo $id; ?>" title="<? echo $i; ?>" class="fill_date<? echo $selected; ?>"><? echo $i; ?></a></td>
<?
		}
	}
	if ($n%7 == 0 && $n != ($first_day+$max_days))
	{
		echo "				</tr>\n";
		echo "				<tr>\n";
	}
}
if (($first_day + $max_days)/7 >= 4 && ($first_day + $max_days)/7 <= 5)
{
	for ($i = $first_day+$max_days; $i <= 35; $i++)
	{
		echo "      <td>&nbsp;</td>\n";
	}
}
else
{
	for ($i = $first_day+$max_days; $i <= 42; $i++)
	{
		echo "      <td>&nbsp;</td>\n";
	}
}
?>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="7">
				<a href="#" title="" class="button close_cal"><? echo $close[$lang]; ?></a>
<?
if ($show_clear)
{
?>
				&nbsp;<a href="#" title="" class="button clear_cal"><? echo $clear[$lang]; ?></a>
<?
}
?>
			</td>
		</tr>
	</tfoot>
</table>