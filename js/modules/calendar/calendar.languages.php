<?

$months["en"] = array(1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June",
					7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");

$days["en"] = array(1 => "Mo", 2 => "Tu", 3 => "We", 4 => "Th", 5 => "Fr", 6 => "Sa",	7 => "Su");

$close["en"] = "close";
$clear["en"] = "clear";

$months["cz"] = array(1 => "Leden", 2 => "�nor", 3 => "B�ezen", 4 => "Duben", 5 => "Kv�ten", 6 => "�erven",
					7 => "�ervenec", 8 => "Srpen", 9 => "Z���", 10 => "��jen", 11 => "Listopad", 12 => "Prosinec");
$days["cz"] = array(1 => "Po", 2 => "�t", 3 => "St", 4 => "�t", 5 => "P�", 6 => "So",	7 => "Ne");

$close["cz"] = "zav��t";
$clear["cz"] = "vymazat";

?>