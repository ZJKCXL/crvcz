$(document).ready(function () {
	userHandler.init();
});

var userHandler = {};
userHandler.init = function ()
{
	$("[name^=newPass]").bind("keyup blur", function (event) {
		$("#newPassStatus").remove();
		
		var elmOther = ($(this).attr("name") == "newPass") ? $("#newPass2") : $("#newPass");
		
		if ($(this).val().length > 0 || elmOther.val().length)
		{
			var result = false;
			var message = "";
		
			if ($(this).val() == elmOther.val())
			{
				if ($(this).val().length < PASSWORD_MINLEN)
				{
					message = JS_PASS_TOO_SHORT;
				}
				else
				{
					result = true;
				
					message = JS_PASS_OK;
				}
			}
			else
			{
				message = JS_PASS_NOT_MATCHING;
			}
		
			var imgSrc = (!result) ? "Cancel" : "Confirm";
		
			$("<span>").attr("id", "newPassStatus")
			.addClass("inlineBlock")
			.css("line-height", "1.8em")
			.append(
				$("<img>").attr("src", BASE_URL+"images/icons/basic/24/"+imgSrc+".png")
				.css("vertical-align", "bottom")
			)
			.append(" "+message)
			.insertAfter($("#newPass2"));
		}
	});
}

form_handler.checkSpecial.frm_passwordChange = function (event) {
	if ($("[name^=newPass]").val() != $("[name^=newPass2]").val())
	{
		alert(JS_PASS_NOT_MATCHING);
		return false;
	}
	
	if ($("[name^=newPass]").val().length < PASSWORD_MINLEN)
	{
		alert(JS_PASS_TOO_SHORT);
		return false;
	}
	
	return true;
};