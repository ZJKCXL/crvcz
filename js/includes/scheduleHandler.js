scheduleHandler = {};
scheduleHandler.init = function () {
	$("a.scheduleDate").bind("click", scheduleHandler.handleClick);
	$("#userID").bind("change", scheduleHandler.changeUser);
};
scheduleHandler.handleClick = function (event) {
	event = $.event.fix(event);
	
	$.fn.statusBar("showWith", {html: JS_TXT_SCHEDULE_SAVING});
	
	var params = {};
	params.url = BASE_URL+"scripts/scheduleHandler.php";
	params.data = {
		dateAt: $(event.target).attr("id").substring(4, $(event.target).attr("id").length),
		userID: $("#userID").val()
	};
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: params.url,
		cache: false,
		data: params.data,
		success: function (response) {
			$("#date"+response.data.dateAt).removeClass();
			$("#date"+response.data.dateAt).addClass("tooltip scheduleDate");
			$("#date"+response.data.dateAt).addClass(response.data.classNew);
			$("#quota"+response.data.dateYM).html(response.data.quota);
			
			if (response.data.tooltip)
			{
				$.fn.tooltip("hide");
				$("#date"+response.data.dateAt).attr("title", response.data.tooltip);
			}
			
			$.fn.statusBar("fadeWith", {html: JS_TXT_SCHEDULE_SAVED});
			$.fn.tooltip("init");
		}
	});
};
scheduleHandler.changeUser = function (event) {
	event = $.event.fix(event);
	if (event.preventDefault) event.preventDefault();
	
	$(event.target).parents("form").submit();
};

$(document).ready(function () {
	scheduleHandler.init();
});