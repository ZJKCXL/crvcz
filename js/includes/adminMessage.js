$(document).ready(function () {
	$(".adminMessageNew").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/messageHandler.php";
		param.data = {
			"act": "new",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		$(".tabs").tabs();
		
		$("textarea.tinymce").tinymce({
			script_url: BASE_URL+"js/tiny_mce/tiny_mce.js",
			content_css : "css/main.css",
			theme: "advanced",

			plugins : "preview,table", 

			// Theme options - button# indicated the row# only
			theme_advanced_buttons1: "cut,copy,paste,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,sub,sup,forecolor,fontsizeselect,formatselect",
			theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,|,removeformat,code,preview,",
			theme_advanced_buttons3: "tablecontrols",

			theme_advanced_toolbar_location: "top",
			theme_advanced_toolbar_align: "left",
			theme_advanced_statusbar_location: "bottom",
			theme_advanced_resizing: false
		});
		
		if (calendar && calendar.init) calendar.init();
	});
	
	$(".adminMessageMod").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();

		var param = {};
		param.url = BASE_URL+"overlays/messageHandler.php";
		param.data = {
			"id": $(this).attr("id").substring(7, $(this).attr("id").length),
			"act": "mod",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		$(".tabs").tabs();
		
		$("textarea.tinymce").tinymce({
			script_url: BASE_URL+"js/tiny_mce/tiny_mce.js",
			content_css : "css/main.css",
			theme: "advanced",

			plugins : "preview,table", 

			// Theme options - button# indicated the row# only
			theme_advanced_buttons1: "cut,copy,paste,|,bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,sub,sup,forecolor,fontsizeselect,formatselect",
			theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,|,removeformat,code,preview,",
			theme_advanced_buttons3: "tablecontrols",

			theme_advanced_toolbar_location: "top",
			theme_advanced_toolbar_align: "left",
			theme_advanced_statusbar_location: "bottom",
			theme_advanced_resizing: false
		});
		
		if (calendar && calendar.init) calendar.init();
	});
});