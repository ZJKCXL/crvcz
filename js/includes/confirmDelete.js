$("#delete").bind("click", function () {
	var postID = $("#frm_confirmDelete input[name^=postID]").val();
	var responseTxt = $("#frm_confirmDelete input[name^=action]").val() == "mark" ? JS_TXT_POST_MARKED : JS_TXT_POST_DELETED;
	
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		url: BASE_URL+"scripts/postDeleteAJAX.php",
		cache: false,
		data: $("#frm_confirmDelete").serializeJSON(),
		success: function (response) {
			if (response.status == "ok")
			{
				$.fn.statusBar("fadeWith", {html: responseTxt});
				if ($("#frm_confirmDelete input[name^=action]").val() == "delete")
				{
					$(".postBlock input[name^=postID]").each(function () {
						if ($(this).val() == postID)
						{
							$(this).parents(".postBlock").remove();
						}
					});
				}
				else
				{
					$(".postBlock input[name^=postID]").each(function () {
						if ($(this).val() == postID)
						{
							$(".newPostMarker", $(this).parents(".postBlock")).remove();
							$(".markForDeletion", $(this).parents(".postBlock")).remove();
							
							$(this).parents(".postBlock").removeClass("newPost");
							$(this).parents(".postBlock").addClass("markedForDeletion");
							
							var html = "<span class=\"markedForDeletionMarker\">"+TXT_MONITORING_DELETE_POST_MARKER+"</span>";
							$(html).insertBefore($("input[name^=md5sum]", $(this).parents(".postBlock")));
						}
					});
				}
			}
			else
			{
				$.fn.statusBar("showWith", {html: JS_TXT_POST_NOT_DELETED});
			}
		}
	});
	
	$.fn.overlay("hide");
});
$("#cancel").bind("click", function () {
	$.fn.overlay("hide");
});