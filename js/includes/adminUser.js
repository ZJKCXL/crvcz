$(document).ready(function () {
	$(".adminUserNew").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/userHandler.php";
		param.data = {
			"act": "new",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		$(".tabs").tabs({
			activate: function(event, ui) {
				event = $.event.fix(event);
			
				if ($(ui.newPanel).attr("id") == "tabs-5")
				{
					$("input[type=submit]", $(this)).css("display", "none");
					$("input[type=submit]", $(ui.newPanel)).css("display", "");
				}
				else
				{
					$("input[type=submit]", $(this)).css("display", "");
				}
			
				var params = {};
				params.resize = "XY";
				$.fn.overlay("resize", params);
			}
		});
		userHandler.init();
	});
	
	$(".adminUserMod").bind("click", function (event) {
		event = $.event.fix(event);
		if (event.preventDefault) event.preventDefault();
			
		var param = {};
		param.url = BASE_URL+"overlays/userHandler.php";
		param.data = {
			"id": $(this).attr("id").substring(4, $(this).attr("id").length),
			"act": "mod",
			"lang": LANGUAGE
		};
			
		$.fn.overlay("show", param);
		$(".tabs").tabs({
			activate: function(event, ui) {
				event = $.event.fix(event);
			
				if ($(ui.newPanel).attr("id") == "tabs-5")
				{
					$("input[type=submit]", $(this)).css("display", "none");
					$("input[type=submit]", $(ui.newPanel)).css("display", "");
				}
				else
				{
					$("input[type=submit]", $(this)).css("display", "");
				}
			
				var params = {};
				params.resize = "XY";
				$.fn.overlay("resize", params);
			}
		});
		userHandler.init();
	});
});

var userHandler = {};
userHandler.init = function ()
{
	$("[name^=newPass]").bind("keyup blur", function (event) {
		$("#newPassStatus").remove();
		
		var elmOther = ($(this).attr("name") == "newPass") ? $("#newPass2") : $("#newPass");
		
		if ($(this).val().length > 0 || elmOther.val().length)
		{
			var result = false;
			var message = "";
		
			if ($(this).val() == elmOther.val())
			{
				if ($(this).val().length < PASSWORD_MINLEN)
				{
					message = JS_PASS_TOO_SHORT;
				}
				else
				{
					result = true;
				
					message = JS_PASS_OK;
				}
			}
			else
			{
				message = JS_PASS_NOT_MATCHING;
			}
		
			var imgSrc = (!result) ? "Cancel" : "Confirm";
		
			$("<span>").attr("id", "newPassStatus")
			.addClass("inlineBlock")
			.css("line-height", "1.8em")
			.append(
				$("<img>").attr("src", BASE_URL+"images/icons/basic/24/"+imgSrc+".png")
				.css("vertical-align", "bottom")
			)
			.append(" "+message)
			.insertAfter($("#newPass2"));
		}
	});
}