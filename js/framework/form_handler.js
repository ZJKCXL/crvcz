var form_handler = {};
form_handler.alert_element_id = [];		//DO NOT ALTER
form_handler.checkSpecial = [];			//DO NOT ALTER
form_handler.invalid_fields_ids = [];	//DO NOT ALTER
form_handler.invalid_fields = [];		//DO NOT ALTER
form_handler.field_types = [];			//DO NOT ALTER
form_handler.default_values = [];		//DO NOT ALTER
form_handler.error_special = [];		//DO NOT ALTER
form_handler.endpointHandler = [];		//DO NOT ALTER

/**
 * Auto init, requires event_handler, class_handler
 *
 * Adds and removes classes 'focused' and 'focused_required' onfocus and onblur
 *
 * Default mode: checkForm with alert output
 * To output invalid fields to a html element:
 * form_handler.alert_element_id.<form_id> = '<element_id>';
 *
 * To setup a special check function for form:
 * form_handler.checkSpecial.<form_id> = function () {};
 *
 * Do the above two in a per-page script include/site wide include, but beware form id collisions!
 */

//Examples:
//	form_handler.alert_element_id.register_form = 'err_field';
//	form_handler.checkSpecial.register_form = function (event) { alert('haha'); };
form_handler.error_field_title = '';
form_handler.alert_by_label = true;

/**
 * Default error message
 *
 * For language variants use constant instead, define elsewhere
 */
form_handler.error_message = 'Some of the form\'s fields are filled in incorrectly, or some of the required fields (marked by red color) are not filled in.';

/**
 * Setup default values for fields
 */

//form_handler.default_values['url'] = 'http://';

/**
 * init function
 */
form_handler.init = function ()
{
	var forms = document.getElementsByTagName('form');

	for (var i = 0; i < forms.length; i++)
	{
		if (class_handler.has(forms[i], 'checkAJAX'))
		{
			event_handler.remove(forms[i], 'submit', form_handler.checkAJAX);
			event_handler.add(forms[i], 'submit', form_handler.checkAJAX);
		}
		else
		{
			event_handler.remove(forms[i], 'submit', form_handler.checkForm);
			event_handler.add(forms[i], 'submit', form_handler.checkForm);
		}

		var inputs = forms[i].getElementsByTagName('input');
		var texts = forms[i].getElementsByTagName('textarea');
		var selects = forms[i].getElementsByTagName('select');

		for (var j = 0; j < inputs.length; j++)
		{
			if (inputs[j].type == 'text' || inputs[j].type == 'password')
			{
				event_handler.remove(inputs[j], 'blur', form_handler.checkField);
				event_handler.remove(inputs[j], 'keyup', form_handler.checkField);
				event_handler.remove(inputs[j], 'focus', form_handler.checkField);
				event_handler.remove(inputs[j], 'change', form_handler.checkField);

				event_handler.remove(inputs[j], 'blur', form_handler.fieldBlur);
				event_handler.remove(inputs[j], 'focus', form_handler.fieldFocus);
				
				event_handler.add(inputs[j], 'blur', form_handler.checkField);
				event_handler.add(inputs[j], 'keyup', form_handler.checkField);
				event_handler.add(inputs[j], 'focus', form_handler.checkField);
				event_handler.add(inputs[j], 'change', form_handler.checkField);

				event_handler.add(inputs[j], 'blur', form_handler.fieldBlur);
				event_handler.add(inputs[j], 'focus', form_handler.fieldFocus);
			}
		}

		for (var j = 0; j < texts.length; j++)
		{
			event_handler.remove(texts[j], 'blur', form_handler.checkField);
			event_handler.remove(texts[j], 'keyup', form_handler.checkField);
			event_handler.remove(texts[j], 'focus', form_handler.checkField);
			event_handler.remove(texts[j], 'change', form_handler.checkField);

			event_handler.remove(texts[j], 'blur', form_handler.fieldBlur);
			event_handler.remove(texts[j], 'focus', form_handler.fieldFocus);
			
			event_handler.add(texts[j], 'blur', form_handler.checkField);
			event_handler.add(texts[j], 'keyup', form_handler.checkField);
			event_handler.add(texts[j], 'focus', form_handler.checkField);
			event_handler.add(texts[j], 'change', form_handler.checkField);

			event_handler.add(texts[j], 'blur', form_handler.fieldBlur);
			event_handler.add(texts[j], 'focus', form_handler.fieldFocus);
		}

		for (var j = 0; j < selects.length; j++)
		{
			event_handler.remove(selects[j], 'blur', form_handler.checkField);
			event_handler.remove(selects[j], 'focus', form_handler.checkField);
			event_handler.remove(selects[j], 'change', form_handler.checkField);

			event_handler.remove(selects[j], 'blur', form_handler.fieldBlur);
			event_handler.remove(selects[j], 'focus', form_handler.fieldFocus);
			
			event_handler.add(selects[j], 'blur', form_handler.checkField);
			event_handler.add(selects[j], 'focus', form_handler.checkField);
			event_handler.add(selects[j], 'change', form_handler.checkField);

			event_handler.add(selects[j], 'blur', form_handler.fieldBlur);
			event_handler.add(selects[j], 'focus', form_handler.fieldFocus);
		}
	}
};
form_handler.buildRegExp = function (string)
{
	if (string)
	{
		return new RegExp("^["+string+"]{1,}$");
	}

	return false;
};
form_handler.checkChars = function (string, re)
{
	if (string && re)
	{
		return (string.search(re) == -1) ? true : false;
	}

	return true;
};
form_handler.checkField = function (event)
{
	if (event.tagName)
	{
		field = event;
	}
	else
	{
		var event = event_handler.fix(event);
		var field = event.target;
	}

	var field_valid = true;

	var classes = class_handler.get(field);

	if (class_handler.has(field, 'required'))
	{
		field_valid = (field.value.length > 0) ? true : false;

		if (field.tagName.toLowerCase() == 'select')
		{
			field_valid = (field.value != -1) ? true : false;
		}
	}

	var i = 0;
	while (field_valid && i < classes.length)
	{
		if (form_handler.default_values[classes[i]])
		{
			if ((event.type == 'focus') && event.target.value.length <= 0)
			{
				field.value = form_handler.default_values[classes[i]];
			}
			if (event.type == 'blur' && field.value == form_handler.default_values[classes[i]])
			{
				field.value = '';
			}
		}
		if (form_handler.field_types[classes[i]])
		{
			field_valid = (field.value.length > 0 && form_handler.checkChars(field.value, form_handler.field_types[classes[i]])) ? false : true;
		}
		i++;
	}

	(field_valid) ? class_handler.remove(field, 'invalid') : class_handler.add(field, 'invalid');

	return (field_valid) ? "valid" : "invalid";
};
form_handler.checkSilent = function (element)
{
	if (typeof(tinyMCE) != "undefined" && tinyMCE && tinyMCE.triggerSave) tinyMCE.triggerSave();
	
	var return_val = true;

	var inputs = element.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++)
	{
		if (form_handler.checkField(inputs[i]) == 'invalid')
		{
			return_val = false;
		}
	}

	var textareas = element.getElementsByTagName('textarea');
	for (var i = 0; i < textareas.length; i++)
	{
		if (form_handler.checkField(textareas[i]) == 'invalid')
		{
			return_val = false;
		}
	}

	var selects = element.getElementsByTagName('select');
	for (var i = 0; i < selects.length; i++)
	{
		if (form_handler.checkField(selects[i]) == 'invalid')
		{
			return_val = false;
		}
	}

	return return_val;
};
form_handler.checkAJAX = function (event)
{
	var event = event_handler.fix(event);

	if (!form_handler.checkForm(event))
	{
		return false;
	}

	var c = 0;
	var param = new Array();

	var inputs = event.target.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++)
	{
		if (inputs[i].tagName.toLowerCase() == 'radio' || inputs[i].tagName.toLowerCase() == 'checkbox')
		{
			if (inputs[i].checked == true || inputs[i].checked == 'true')
			{
				temp_field = new Array(inputs[i].name, inputs[i].value);
				param[c++] = temp_field;
			}
		}
		else
		{
			temp_field = new Array(inputs[i].name, inputs[i].value);
			param[c++] = temp_field;
		}
	}

	var textareas = event.target.getElementsByTagName('textarea');
	for (var i = 0; i < textareas.length; i++)
	{
		temp_field = new Array(textareas[i].name, textareas[i].value);
		param[c++] = temp_field;
	}

	var selects = event.target.getElementsByTagName('select');
	for (var i = 0; i < selects.length; i++)
	{
		temp_field = new Array(selects[i].name, selects[i].value);
		param[c++] = temp_field;
	}

	if (requester.loadURL('POST', form_handler.checkAJAX_script, param, false))
	{
		var response = requester.getXML();

		if (1 == 1)
		{
			return false;
		}
	}

	return true;
};
form_handler.getInvalidFields = function (event)
{
	form_handler.invalid_fields_ids = [];
	form_handler.invalid_fields = [];

	event = event_handler.fix(event);
	var form = event.target;

	var inputs = form.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++)
	{
		if (class_handler.has(inputs[i], 'invalid'))
		{
			form_handler.invalid_fields_ids[form_handler.invalid_fields_ids.length] = inputs[i].id;
		}
	}

	var selects = form.getElementsByTagName('select');
	for (var i = 0; i < selects.length; i++)
	{
		if (class_handler.has(selects[i], 'invalid'))
		{
			form_handler.invalid_fields_ids[form_handler.invalid_fields_ids.length] = selects[i].id;
		}
	}

	var textareas = form.getElementsByTagName('textarea');
	for (var i = 0; i < textareas.length; i++)
	{
		if (class_handler.has(textareas[i], 'invalid'))
		{
			form_handler.invalid_fields_ids[form_handler.invalid_fields_ids.length] = textareas[i].id;
		}
	}

	if (form_handler.alert_by_label)
	{
		var labels = form.getElementsByTagName('label');
		for (var i = 0; i < labels.length; i++)
		{
			var tmp_for = labels[i].getAttribute('for');
			if (tmp_for)
			{
				var tmp_index = form_handler.invalid_fields_ids.indexOf(tmp_for);
				if (tmp_index >= 0)
				{
					var tmp_desc = labels[i].innerHTML;

					form_handler.invalid_fields[form_handler.invalid_fields.length] = tmp_desc;
				}
			}
		}
	}
	else
	{
		for (var i = 0; i < form_handler.invalid_fields_ids.length; i++)
		{
			form_handler.invalid_fields[i] = document.getElementById(form_handler.invalid_fields_ids[i]).title;
		}
	}

	var tmp_err_text = '';

	for (var i = 0; i < form_handler.invalid_fields.length; i++)
	{
		tmp_err_text += (i > 0) ? ', ' : '';
		tmp_err_text += form_handler.invalid_fields[i];
	}

	return tmp_err_text;
};
form_handler.checkForm = function (event)
{
	var event = event_handler.fix(event);
	var target_form = event.target;

	if (!form_handler.checkSilent(target_form))
	{
		event.preventDefault();

		if (target_form.id)
		{
			eval('var tmp_handler = form_handler.alert_element_id.'+target_form.id);
			if (tmp_handler)
			{
				var tmp_element = document.getElementById(tmp_handler);
				if (tmp_element)
				{
					tmp_element.innerHTML = form_handler.error_field_title+form_handler.getInvalidFields(event);
					tmp_element.style.visibility = 'visible';
					tmp_element.style.display = 'block';
					return false;
				}
			}
		}

		alert(form_handler.error_message);
		return false;
	}
	else
	{
		if (target_form.id)
		{
			eval('var tmp_handler = form_handler.alert_element_id.'+target_form.id);
			if (tmp_handler)
			{
				var tmp_element = document.getElementById(tmp_handler);
				if (tmp_element)
				{
					tmp_element.innerHTML = '';
					tmp_element.style.display = 'none';
				}
			}
		}
	}

	if (target_form.id)
	{
		eval('var tmp_handler = form_handler.checkSpecial.'+target_form.id);
		if (tmp_handler)
		{
			eval('var check_res = form_handler.checkSpecial.'+target_form.id+'(event)');
			if (!check_res)
			{
				eval('var tmp_error = form_handler.error_special.'+target_form.id);
				if (!tmp_error)
				{
					event.preventDefault();

					return false;
				}

				eval('var tmp_handler = form_handler.alert_element_id.'+target_form.id);
				if (tmp_handler)
				{
					var tmp_element = document.getElementById(tmp_handler);
					if (tmp_element && tmp_error)
					{
						tmp_element.innerHTML = form_handler.error_field_title+tmp_error;
						tmp_element.style.visibility = 'visible';
						tmp_element.style.display = 'block';
					}
				}
				else
				{
					alert(tmp_error);
				}

				event.preventDefault();

				return false;
			}
		}
	}
	
	eval('var tmp_endpoint_handler = form_handler.endpointHandler.'+target_form.id);
	if (tmp_endpoint_handler)
	{
		if ($("#"+target_form.id).hasClass("json"))
		{
			var target_url = $("#"+target_form.id).attr("action");
			if (target_url.length <= 0)
			{
				target_url = $("#"+target_form.id+" input[name=endpoint]").val();
			}
			
			$.ajax({
				type: "POST",
				dataType: "json",
				async: false,
				url: target_url,
				cache: false,
				data: {
					ajax: 1,
					data: $("#"+target_form.id).serializeJSON()
				},
				success: function (json)
				{
					tmp_endpoint_handler(json);
				}
			});
			
			event.preventDefault();
			
			return false;
		}
		else
		{
			/*if (!tmp_endpoint_handler())
			{
				event.preventDefault();
			
				return false;
			}*/
		}
	}

	return true;
};
form_handler.fieldFocus = function (event)
{
	var event = event_handler.fix(event);

	if (!class_handler.has(event.target, 'focused'))
	{
		if (class_handler.has(event.target, 'required'))
		{
			class_handler.add(event.target, 'focused_reqired');
		}
		else
		{
			class_handler.add(event.target, 'focused');
		}
	}
};
form_handler.fieldBlur = function (event)
{
	var event = event_handler.fix(event);

	class_handler.remove(event.target, 'focused');
	class_handler.remove(event.target, 'focused_reqired');
};

/**
 * Setup RegExp library below
 *
 * Use form_handler.buildRegExp() for building RegExp from a set of items
 */
form_handler.field_types["numbers"] =				form_handler.buildRegExp("0123456789");
form_handler.field_types["integer"] =				new RegExp("^([-]|())([0123456789]{1,})$");
form_handler.field_types["float"] =					new RegExp("^([-]|())([0123456789]{1,})(()|(([\054])([0123456789]{1,})))$");

form_handler.field_types["safe_chars"] =			form_handler.buildRegExp("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_");

form_handler.field_types["alphabet_lowercase"] =	form_handler.buildRegExp("abcdefghijklmnopqrstuvwxyzáčďéěíňóřšťúůťýžüöäëß ");
form_handler.field_types["alphabet_uppercase"] =	form_handler.buildRegExp("ABCDEFGHIJKLMNOPQRSTUVWXYZÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽÜÖÄËß ");
form_handler.field_types["alphabet"] =				form_handler.buildRegExp("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß ");
form_handler.field_types["alphanumeric"] =			form_handler.buildRegExp("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽ0123456789üöäëÜÖÄËß ");

form_handler.field_types["extended"] =				form_handler.buildRegExp("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáčďéěíňóřšťúůťýžÁČĎÉĚÍŇÓŘŠŤÚŮŤÝŽüöäëÜÖÄËß\n\r\040\041\042\043\044\045\046\047\050\051\052\053\054\055\056\057\072\073\074\075\076\077\100\134\137\140\173\175");

form_handler.field_types["phone_international"] =	new RegExp("^((([+][1-9][0-9]{2}([ ]{0,1}))|([0]{2}([ ]{0,1})[1-9][0-9]{2}([ ]{0,1})))|())([1-9]{1}[0-9]{2}([ ]{0,1})[0-9]{3}([ ]{0,1})[0-9]{3})$");

form_handler.field_types["url"] =					new RegExp("^([htt]+(p|s))|[ftp]+[:]\/\/[a-zA-Z0-9]+([-_\.]?[a-zA-Z0-9])*\.[a-zA-Z]{2,4}(\/{1}[-_~&=\?\.a-z0-9]*)*$");

form_handler.field_types["email"] =					new RegExp("^[a-z0-9]+[a-z0-9\._-]*[a-z0-9]+@[a-z0-9]+[a-z0-9\._-]*[a-z0-9]+\.[a-z]{2,4}$");

form_handler.field_types["us_date"] =				new RegExp("^(([0-9]{4})([-])((0[1-9])|(1[012]))([-])((0[1-9])|([12][0-9])|(3[01])))$");
form_handler.field_types["en_date"] =				form_handler.field_types["us_date"];

form_handler.field_types["us_datetime"] =			new RegExp("^((([0-9]{4})([-])((0[1-9])|(1[012]))([-])((0[1-9])|([12][0-9])|(3[01])))([ ]{1})(((0[0-9])|(1[0-9])|(2[0-3]))([:])([0-5][0-9])([:])([0-5][0-9])))$");
form_handler.field_types["en_datetime"] =			form_handler.field_types["us_datetime"];

form_handler.field_types["cz_date"] =				new RegExp("^(((0[1-9]|[12][0-9]|3[01])(\.)(0[13578]|10|12)(\.)([1-2][0,9][0-9][0-9]))|(([0][1-9]|[12][0-9]|30)(\.)(0[469]|11)(\.)([1-2][0,9][0-9][0-9]))|((0[1-9]|1[0-9]|2[0-8])(\.)(02)(\.)([1-2][0,9][0-9][0-9]))|((29)(\.)(02)(\.)([02468][048]00))|((29)(\.)(02)(\.)([13579][26]00))|((29)(\.)(02)(\.)([0-9][0-9][0][48]))|((29)(\.)(02)(\.)([0-9][0-9][2468][048]))|((29)(\.)(02)(\.)([0-9][0-9][13579][26])))$");
form_handler.field_types["cs_date"] =				form_handler.field_types["cz_date"];

form_handler.field_types["time"] =					new RegExp("^(((0[0-9])|(1[0-9])|(2[0-3]))([:])([0-5][0-9]))$");

event_handler.add(window, 'load', form_handler.init);