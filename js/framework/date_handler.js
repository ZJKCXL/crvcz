var date_handler = {};
date_handler.compare = function (from_date, to_date, sharp)
{
	start_date = new Array();
	end_date = new Array();

	start_date['year'] = from_date.substring(0, 4);
	start_date['month'] = from_date.substring(5, 7);
	start_date['day'] = from_date.substring(8, 10);
	start_date['hours'] = (from_date.substring(11, 13) != '') ? from_date.substring(11, 13) : 0;
	start_date['minutes'] = (from_date.substring(14, 16) != '') ? from_date.substring(14, 16) : 0;
	start_date['seconds'] = (from_date.substring(17, 19) != '') ? from_date.substring(17, 19) : 0;

	start_date['year'] = new Number(start_date['year']).valueOf();
	start_date['month'] = new Number(start_date['month']).valueOf()-1;
	start_date['day'] = new Number(start_date['day']).valueOf();
	start_date['hours'] = new Number(start_date['hours']).valueOf();
	start_date['minutes'] = new Number(start_date['minutes']).valueOf();
	start_date['seconds'] = new Number(start_date['seconds']).valueOf();

	end_date['year'] = to_date.substring(0, 4);
	end_date['month'] = to_date.substring(5, 7);
	end_date['day'] = to_date.substring(8, 10);
	end_date['hours'] = (to_date.substring(11, 13) != '') ? to_date.substring(11, 13) : 0;
	end_date['minutes'] = (to_date.substring(14, 16) != '') ? to_date.substring(14, 16) : 0;
	end_date['seconds'] = (to_date.substring(17, 19) != '') ? to_date.substring(17, 19) : 0;

	end_date['year'] = new Number(end_date['year']).valueOf();
	end_date['month'] = new Number(end_date['month']).valueOf()-1;
	end_date['day'] = new Number(end_date['day']).valueOf();
	end_date['hours'] = new Number(end_date['hours']).valueOf();
	end_date['minutes'] = new Number(end_date['minutes']).valueOf();
	end_date['seconds'] = new Number(end_date['seconds']).valueOf();

	var from_date = Date.UTC(start_date['year'],start_date['month'],start_date['day'],start_date['hours'],start_date['minutes'],start_date['seconds'],0);

	var to_date = Date.UTC(end_date['year'],end_date['month'],end_date['day'],end_date['hours'],end_date['minutes'],end_date['seconds'],0);

	if (sharp == true)
	{
		if (from_date < to_date)
		{
			return true;
		}
	}
	else
	{
		if (from_date <= to_date)
		{
			return true;
		}
	}

	return false;
};
date_handler.compareCZ = function (from_date, to_date, sharp)
{
	start_date = new Array();
	end_date = new Array();

	start_date['year'] = from_date.substring(6, 10);
	start_date['month'] = from_date.substring(3, 5);
	start_date['day'] = from_date.substring(0, 2);
	start_date['hours'] = (from_date.substring(11, 13) != '') ? from_date.substring(11, 13) : 0;
	start_date['minutes'] = (from_date.substring(14, 16) != '') ? from_date.substring(14, 16) : 0;
	start_date['seconds'] = (from_date.substring(17, 19) != '') ? from_date.substring(17, 19) : 0;

	start_date['year'] = new Number(start_date['year']).valueOf();
	start_date['month'] = new Number(start_date['month']).valueOf()-1;
	start_date['day'] = new Number(start_date['day']).valueOf();
	start_date['hours'] = new Number(start_date['hours']).valueOf();
	start_date['minutes'] = new Number(start_date['minutes']).valueOf();
	start_date['seconds'] = new Number(start_date['seconds']).valueOf();

	end_date['year'] = to_date.substring(6, 10);
	end_date['month'] = to_date.substring(3, 5);
	end_date['day'] = to_date.substring(0, 2);
	end_date['hours'] = (to_date.substring(11, 13) != '') ? to_date.substring(11, 13) : 0;
	end_date['minutes'] = (to_date.substring(14, 16) != '') ? to_date.substring(14, 16) : 0;
	end_date['seconds'] = (to_date.substring(17, 19) != '') ? to_date.substring(17, 19) : 0;

	end_date['year'] = new Number(end_date['year']).valueOf();
	end_date['month'] = new Number(end_date['month']).valueOf()-1;
	end_date['day'] = new Number(end_date['day']).valueOf();
	end_date['hours'] = new Number(end_date['hours']).valueOf();
	end_date['minutes'] = new Number(end_date['minutes']).valueOf();
	end_date['seconds'] = new Number(end_date['seconds']).valueOf();

	var from_date = Date.UTC(start_date['year'],start_date['month'],start_date['day'],start_date['hours'],start_date['minutes'],start_date['seconds'],0);

	var to_date = Date.UTC(end_date['year'],end_date['month'],end_date['day'],end_date['hours'],end_date['minutes'],end_date['seconds'],0);

	if (sharp == true)
	{
		if (from_date < to_date)
		{
			return true;
		}
	}
	else
	{
		if (from_date <= to_date)
		{
			return true;
		}
	}

	return false;
};
date_handler.diff = function (date_start, date_end)
{
	arr_start = date_start.split('-');
	arr_end = date_end.split('-');

	var tmp_start = new Date(arr_start[0], arr_start[1], arr_start[2]);
	var tmp_end = new Date(arr_end[0], arr_end[1], arr_end[2]);

	var modifier = 1000*60*60*24;

	return Math.ceil((tmp_end.getTime()-tmp_start.getTime())/modifier);
};
date_handler.add = function (date_str, add_sec)
{
	var result = "";

	date_str = date_str.split("-");

	var tmp_date = Date.UTC(date_str[0], date_str[1]-1, date_str[2]);
	tmp_date += add_sec*1000;

	var new_date = new Date();
	new_date.setTime(tmp_date);

	result = new_date.getUTCFullYear()+"-";

	month = ("0"+(new_date.getUTCMonth()+1));
	month = month.substring(month.length-2, month.length);
	result += month+"-";

	day = ("0"+new_date.getUTCDate());
	day = day.substring(day.length-2, day.length);
	result += day;

	return result;
};
date_handler.convert = function (date, format_in, format_out)
{
	var date = date.split(/[^\w]/);

	var format_in = format_in.split(/[^\w]/);
	for (var i = 0; i < format_in.length; i++)
	{
		switch (format_in[i])
		{
			case "d":
			case "D":
				var day = date[i];
				break;
			case "m":
			case "M":
				var month = date[i];
				break;
			case "y":
			case "Y":
				var year = date[i];
				break;
		}
	}

	var date_out = "";

	for (var i = 0; i < format_out.length; i++)
	{
		switch (format_out[i])
		{
			case "d":
				day = ('0'+day);
				day = day.substring(day.length-2, day.length);
				date_out += day;
				break;
			case "m":
				month = ('0'+month);
				month = month.substring(month.length-2, month.length);
				date_out += month;
				break;
			case "Y":
				year = ('20'+year);
				year = year.substring(year.length-4, year.length);
				date_out += year;
				break;
			default:
				date_out += format_out[i];
				break;
		}
	}

	return date_out;
};

var timeHandler = {};
timeHandler.normalizeTime = function (event) {
	event = $.event.fix(event);
	
	var newval = $(event.target).val();
			
	if (newval.length <= 0)
	{
		return;
	}
			
	var hrs = 0;
	var mins = 0;
	var newvalSplit = newval.split(/[^\d]+/);
	if (newvalSplit.length > 1)
	{
		hrs = new Number(newvalSplit[0]).valueOf();
		mins = new Number(newvalSplit[1]).valueOf();
	}
	else
	{
		switch (newval.length)
		{
			case 0:
				hrs = newval;
				break;
			case 1:
				hrs = sprintf("%02d:00", parseInt(newval));
				break;
			case 2:
				hrs = parseInt(newval) >= 24 ? 0 : parseInt(newval);
				break;
			case 3:
				var tmp = new String(newval).valueOf();
				var tmpArr = [];
				hrs = new Number(tmp[0]).valueOf();
				mins = new Number(tmp.substring(1, 3)).valueOf();
				break;
			case 4:
			default:
				var tmp = new String(newval).valueOf();
				var tmpArr = [];
				hrs = new Number(tmp.substring(0, 2)).valueOf();
				mins = new Number(tmp.substring(2, 4)).valueOf();
				break;
		}
	}
			
	hrs = hrs >= 24 ? 0 : hrs;
	mins = mins > 59 ? 59 : mins;

	newval = sprintf("%02d", hrs);
	newval += ":";
	newval += sprintf("%02d", mins);
		
	$(event.target).val(newval);
	if (typeof(form_handler) != "undefined" && form_handler.checkField) form_handler.checkField(this);
};