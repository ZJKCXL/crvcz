position = {};
position.getScrollX = function (event)
{
	if (typeof(window.pageYOffset) == 'number')
	{
		return window.pageXOffset;
	}
	else if (document.body && (document.body.scrollLeft || document.body.scrollTop))
	{
		return document.body.scrollLeft;
	}
	else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop))
	{
		return document.documentElement.scrollLeft;
	}
	return 0;
};
position.getScrollY = function (event)
{
	if (typeof(window.pageYOffset) == 'number')
	{
		return window.pageYOffset;
	}
	else if (document.body && (document.body.scrollLeft || document.body.scrollTop))
	{
		return document.body.scrollTop;
	}
	else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop))
	{
		return document.documentElement.scrollTop;
	}
	return 0;
};
position.getAvailableHeight = function ()
{
	if (typeof(window.innerHeight) == 'number')
	{
		return window.innerHeight;
	}
	else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
	{
		return document.documentElement.clientHeight;
	}
	else if (document.body && (document.body.clientWidth || document.body.clientHeight))
	{
		return document.body.clientHeight;
	}
};
position.getAvailableWidth = function ()
{
	if (typeof(window.innerWidth) == 'number')
	{
		return window.innerWidth;
	}
	else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
	{
		return document.documentElement.clientWidth;
	}
	else if (document.body && (document.body.clientWidth || document.body.clientHeight))
	{
		return document.body.clientWidth;
	}
};
position.getPositionX = function (event)
{
	var return_value = 0;

	if (event.pageX)
	{
		return_value = event.pageX;
	}
	if (event.clientX)
	{
		return_value = event.clientX + position.getScrollX(event);
	}

	return return_value;
};
position.getPositionY = function (event)
{
	var return_value = 0;

	if (event.pageY)
	{
		return_value = event.pageY;
	}
	if (event.clientY)
	{
		return_value = event.clientY + position.getScrollY(event);
	}

	return return_value;
};
position.getOffsetX = function (object)
{
	var tmp_left = 0;
	if (object.offsetParent)
	{
		do
		{
			tmp_left += object.offsetLeft;
		} while (object = object.offsetParent);
	}

	return tmp_left;
};
position.getOffsetY = function (object)
{
	var tmp_top = 0;
	if (object.offsetParent)
	{
		do
		{
			tmp_top += object.offsetTop;
		} while (object = object.offsetParent);
	}

	return tmp_top;
};
position.getOuterWidth = function (object, with_margin)
{
	var retval = 0;
	if (object.tagName)
	{
		retval += object.offsetWidth;
		retval += (object.style.borderWidth) ? parseInt(object.style.borderWidth) : 0;

		if (with_margin)
		{
			retval += (object.style.margin) ? parseInt(object.style.margin) : 0;
		}
	}

	return retval;
};
position.getOuterHeight = function (object, with_margin)
{
	var retval = 0;
	if (object.tagName)
	{
		retval += object.offsetHeight;
		retval += (object.style.borderWidth) ? parseInt(object.style.borderWidth) : 0;

		if (with_margin)
		{
			retval += (object.style.margin) ? parseInt(object.style.margin) : 0;
		}
	}

	return retval;
};
position.getMaxWidth = function ()
{
    return Math.max(
        Math.max(document.body.scrollWidth, document.documentElement.scrollWidth),
        Math.max(document.body.offsetWidth, document.documentElement.offsetWidth),
        Math.max(document.body.clientWidth, document.documentElement.clientWidth)
    );
};
position.getMaxHeight = function ()
{
    return Math.max(
        Math.max(document.body.scrollHeight, document.documentElement.scrollHeight),
        Math.max(document.body.offsetHeight, document.documentElement.offsetHeight),
        Math.max(document.body.clientHeight, document.documentElement.clientHeight)
    );
};