/*
find actual version of this library at:
http://www.fczbkk.com/js/dom/
*/

// library for cross-browser event management
evt = {

	// attach event
	add : function(obj, evType, fn, useCapture) {
		// Opera hack
		if (window.opera && (obj == window)) {
			obj = document;
		}
		
		if (obj.addEventListener){
			obj.addEventListener(evType, fn, useCapture);
			return true;
		} else if (obj.attachEvent){
			var r = obj.attachEvent("on"+evType, fn);
			return r;
		} else {
			return false;
		}
	},
	
	// remove event
	remove : function(obj, evType, fn, useCapture) {
		// Opera hack
		if (window.opera && (obj == window)) {
			obj = document;
		}
		
		if (obj.removeEventListener) {
			obj.removeEventListener(evType, fn, useCapture);
			return true;
		} else if (obj.detachEvent) {
			var r = obj.detachEvent("on"+evType, fn);
			return r;
		} else {
			return false;
		}
	},
	
	// fix for IE event model
	fix : function(e) {
		if (typeof e == 'undefined') e = window.event;
		if (typeof e.target == 'undefined') e.target = e.srcElement;
		if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
		if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
		if ((typeof e.which == 'undefined') && e.keyCode) e.which = e.keyCode;

		// thanx to KKL2401 for preventDefault hack
		if (!e.preventDefault) e.preventDefault = function() {
			e.returnValue = false;
		}

		return e;
	}

}



// library for working with multiple classes
var cls = {
	
	// vrati pole obsahujuce vsetky triedy daneho elementu
	get : function (elm) {
		if (elm && elm.tagName) {
			var classes = [];
			if (elm.className) {	// na zaklade Centiho upozornenia o divnej interpretacii v Opere
				var cl = elm.className.replace(/\s+/g, " ");
				classes = cl.split(" ");
			}
			return classes;
		}
		return false;
	},
	
	// vrati true, ak element obsahuje triedu
	has : function (elm, cl) {
		if ((actCl = cls.get(elm)) && (typeof(cl) == "string")) {
			for (var i = 0; i < actCl.length; i++) {
				if (actCl[i] == cl) {
					return true;
				}
			}
		}
		return false;
	},
	
	// prida triedu elementu
	add : function (elm, cl) {
		if ((actCl = cls.get(elm)) && (typeof(cl) == "string")) {
			if (!cls.has(elm, cl)) {
				elm.className += (actCl.length > 0) ? " " + cl : cl;
			}
			return true;
		}
		return false;
	},
	
	// odstrani triedu z elementu
	remove : function (elm, cl) {
		if ((actCl = cls.get(elm)) && (typeof(cl) == "string")) {
			tempCl = "";
			for (var i = 0; i < actCl.length; i++) {
				if (actCl[i] != cl) {
					if (tempCl != "") {tempCl += " ";}
					tempCl += actCl[i];
				}
				elm.className = tempCl;
			}
			return true;
		}
		return false;
	},
	
	// nahradi staru triedu elementu novou, ak stara neexistuje, prida novu
	replace : function (elm, oldCl, newCl) {
		if ((actCl = cls.get(elm)) && (typeof(oldCl) == "string") && (typeof(newCl) == "string")) {
			tempCl = "";
			if (cls.has(elm, newCl)) {
				cls.remove(elm, oldCl);
			} else if (cls.has(elm, oldCl)) {
				for (var i = 0; i < actCl.length; i++) {
					if (tempCl != "") {tempCl += " ";}
					tempCl += (actCl[i] == oldCl) ? newCl : actCl[i];
				}
				elm.className = tempCl;
			} else {
				cls.add(elm, newCl);
			}
			return true;
		}
		return false;
	}

}