<p>Pro vkládání textů nepoužívejte prosím přímé kopírování (Copy/Paste) z textových editorů typu <strong>Microsoft Word</strong><br/>
Data kopírovaná do schránky mohou obsahovat (často nestandardní) formátovací příkazy, které se systém pokusí interpretovat a může dojít k nežádoucímu formátování textů po zveřejnění na webu</p>
<p>Pro případ, kdy je neefektivní text vytvořit přímo v systému HandMade, můžete text, předem napsaný např. ve Wordu importovat do pole pro vložení textu prostřednictvím funkce <img src="./images/pastetext.gif" border="0"><strong> Copy Text</strong>, nebo <img src="./images/pasteword.gif" border="0"><strong> Copy text from Word</strong>. Prvním způsobem vytvoříte čistější html kód. 
</p>
<p>
Děkujeme
</p>
